import pymongo
import logging
from config import *

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

# Get a Client to the MongoDB instance
client = pymongo.MongoClient(DB_HOST,DB_PORT)
dbEyetalk = client.eyetalk
gazeColl = dbEyetalk.gazepoints

for line in open("eyedata.txt"):
    line = line.strip()
    line = line[1:len(line)-1]
    entries = line.split(",")
    gazeColl.insert({'ts':entries[0].strip(), 'x':entries[1].strip(), 'y':entries[2].strip()})
