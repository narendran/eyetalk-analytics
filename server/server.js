var socket = require('socket.io')();

// Creating the websocket server for clients to connect
socket.on('connection', function(socket){
    console.log("Connection created! from " + socket)
    socket.on('disconnect', function() {
        console.log("Client disconnected")
    });
});
socket.listen(5001);

var MongoWatch = require('mongo-watch')
var watcher = new MongoWatch({parser: 'pretty', useMasterOplog: true});

watcher.watch('eyehome.gazepoints', function(event) {
    // console.log("Event : " + event)
    socket.emit("message", {data : event})
});
