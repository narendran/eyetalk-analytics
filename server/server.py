import pymongo
import logging
import json
from crossdomain import *
from config import *
from flask import Flask, request
app = Flask(__name__, static_url_path='/static')

client = pymongo.MongoClient()
dbeyeHome = client.eyehome
gazeColl = dbeyeHome.gazepoints

@app.route("/")
def root():
    return "Testing - Flask works!"

@app.route("/analytics")
def analytics():
    return app.send_static_file('analytics.html')

@app.route("/realtime")
def rtanalytics():
    return app.send_static_file('realtimegaze.html')

# TODO : Remove harcoding. Check Flask Bundles
@app.route("/heatmap.min.js")
def heatmap():
    return app.send_static_file('heatmap.min.js')

@app.route("/appnames")
@crossdomain(origin='*')
def appnames():
    response = []
    response.append("all")
    response.extend(gazeColl.distinct('application'))
    return json.dumps(response)

@app.route("/gazepoints/<app>")
@crossdomain(origin='*')
def gazepoints(app):
    map = 'function() { var key = this.x + "," +this.y + "," + this.application; emit(key, {count : 1}); }'
    reduce = 'function(key, values) { var total = 0; for ( var i = 0; i < values.length; i++) { total += values[i].count;} return {count: total}; }'
    result = gazeColl.map_reduce(map,reduce, {'inline':1})
    result = result['results'] #Since its an inline mapreduce query
    response = []
    if app == "all":
        for doc in result:
            res_tuple = {}
            res_tuple['x'] = doc['_id'].split(",")[0]
            res_tuple['y'] = doc['_id'].split(",")[1]
            res_tuple['count'] = doc['value']['count']
            response.append(res_tuple)
    else:
        for doc in result:
            res_tuple = {}
            res_tuple['x'] = doc['_id'].split(",")[0]
            res_tuple['y'] = doc['_id'].split(",")[1]
            res_tuple['count'] = doc['value']['count']
            if doc['_id'].split(",")[2] == app:
                response.append(res_tuple)   
    return json.dumps(response)

if __name__ == "__main__":
    app.debug = True
    app.run('0.0.0.0')
